const { GraphQLObjectType, GraphQLString } = require('graphql')

const UserType = new GraphQLObjectType({
  name: 'Location',
  description: '...',

  fields: () => ({
    name: {
      type: GraphQLString,
      resolve: user => user.name,
    },
  }),
})

module.exports = UserType;
