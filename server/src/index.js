const express = require('express')
const bodyParser = require('body-parser')
const fetch = require('node-fetch')
const { graphqlExpress, graphiqExpress } = require('apollo-server-express')
const myGraphQLSchema = require('./graphql/Schema')
const neo4j = require('node-neo4j');
const db = new neo4j('http://neo4j:8nvMWChr8kBkRStS@neo4j:7474')

const app = express()


app.get('/', (req, res) => {
  res.json({ ok: 200 })
})

app.get('/insert', (req, res, next) => {
    db.insertNode({
        name: 'Darth Vader #' + parseInt(Math.random() * 100),
        sex: 'male'
    }, ['Person'], (err, node) => {
        if (err) return next(err)

        res.json(node)
    })
})

app.get('/persons', (req, res, next) => {
    db.cypherQuery("MATCH (person:Person) RETURN person", function (err, result) {
        if (err) return next(err);
        res.json(result.data);
    });
});

app.listen(3000, () => console.log('Server running on port 3000!'))
